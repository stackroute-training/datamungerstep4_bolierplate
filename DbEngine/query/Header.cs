namespace DbEngine.Query
{
    public class Header
    {
        /*
	 * This class should contain a property named as Headers which is a String array, to hold
	 * the headers and should override toString() method as well. Initialize this property in constructor
	 */
        public string[] Headers { get; set; }

        //implement constructor and override tostring method
        public Header(string id, string season, string city, string date, string team1, string team2, string tossWinner, string tDesci, string result, string dl_applied, string winner, string winByRuns, string winByWick, string pom, string venue, string u1, string u2, string u3)
        {
            Headers = new[] { id, season, city, date, team1, team2, tossWinner, tDesci, result, dl_applied, winner, winByRuns, winByWick, pom, venue, u1, u2, u3 };
        }
    }
}